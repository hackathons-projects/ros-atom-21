package ru.rosatom.rosatomcrmback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RosAtomCrmBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(RosAtomCrmBackApplication.class, args);
    }

}
