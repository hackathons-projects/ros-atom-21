package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;
import ru.rosatom.rosatomcrmback.model.UserDto;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class UserEntityConverter implements Converter<UserEntity, UserDto> {

    @Override
    public UserDto convert(MappingContext<UserEntity, UserDto> mappingContext) {
        UserDto userDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new UserDto());
        UserEntity userEntity = mappingContext.getSource();
        userDto.setId(userEntity.getId());
        userDto.setBirthDay(userEntity.getBirthDay());
        userDto.setEmail(userEntity.getEmail());
        userDto.setDepartmentId(userEntity.getDepartment().getId());
        userDto.setIsOnline(userEntity.getIsOnline());
        userDto.setName(userEntity.getName());
        userDto.setLastName(userEntity.getLastName());
        userDto.setMiddleName(userEntity.getMiddleName());
        userDto.setRole(userEntity.getRole());
        userDto.setManagerId(Optional.ofNullable(userEntity.getManager()).map(u -> u.getId()).orElse(null));
        userDto.setPhone(userEntity.getPhone());
        userDto.setPosition(userEntity.getPosition());
        userEntity.getPersonal().forEach(user -> userDto.getPersonalIds().add(user.getId()));
        userEntity.getTasks().forEach(task -> userDto.getTasksIds().add(task.getId()));
        return userDto;
    }

}
