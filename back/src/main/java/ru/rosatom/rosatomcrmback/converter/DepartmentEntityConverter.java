package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;
import ru.rosatom.rosatomcrmback.model.DepartmentDto;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class DepartmentEntityConverter implements Converter<DepartmentEntity, DepartmentDto> {

    @Override
    public DepartmentDto convert(MappingContext<DepartmentEntity, DepartmentDto> mappingContext) {
        DepartmentDto departmentDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new DepartmentDto());
        DepartmentEntity department = mappingContext.getSource();
        departmentDto.setId(department.getId());
        departmentDto.setName(department.getName());
        departmentDto.setDescription(department.getDescription());
        departmentDto.setDirectorId(Optional.ofNullable(department.getDirector()).map(userEntity -> userEntity.getId()).orElse(null));
        department.getPersonal().forEach(userEntity -> departmentDto.getPersonalIds().add(userEntity.getId()));
        department.getTasks().forEach(task -> departmentDto.getTasksIds().add(task.getId()));
        return departmentDto;
    }
}
