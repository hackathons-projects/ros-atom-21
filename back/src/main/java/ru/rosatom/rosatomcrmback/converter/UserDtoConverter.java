package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.TaskRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.UserDto;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class UserDtoConverter implements Converter<UserDto, UserEntity> {

    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;
    private final TaskRepository taskRepository;

    @Override
    public UserEntity convert(MappingContext<UserDto, UserEntity> mappingContext) {
        UserEntity userEntity = Optional.ofNullable(mappingContext.getDestination()).orElse(new UserEntity());
        UserDto userDto = mappingContext.getSource();
        userEntity.setName(userDto.getName());
        userEntity.setLastName(userDto.getLastName());
        userEntity.setMiddleName(userDto.getMiddleName());
        userEntity.setBirthDay(userDto.getBirthDay());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setIsOnline(userDto.getIsOnline());
        userEntity.setManager(userRepository.findById(userDto.getManagerId()).orElse(null));
        userEntity.setDepartment(departmentRepository.findById(userDto.getDepartmentId()).orElse(null));
        userEntity.setPhone(userDto.getPhone());
        userEntity.setPosition(userDto.getPosition());
        userRepository.findAllById(userDto.getPersonalIds()).forEach(user -> userEntity.getPersonal().add(user));
        taskRepository.findAllById(userDto.getTasksIds()).forEach(task -> userEntity.getTasks().add(task));
        return userEntity;
    }
}
