package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;
import ru.rosatom.rosatomcrmback.domain.model.NodeEntity;
import ru.rosatom.rosatomcrmback.model.NodeDto;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class NodeEntityConverter implements Converter<NodeEntity, NodeDto> {

    private static final long VISIBILITY_THRESHOLD = 3L;

    @Override
    public NodeDto convert(MappingContext<NodeEntity, NodeDto> mappingContext) {
        NodeEntity nodeEntity = mappingContext.getSource();

        NodeEntity parent = nodeEntity.getParent();
        return new NodeDto(
                nodeEntity.getId(),
                nodeEntity.getQuestion(),
                nodeEntity.getBundleText(),
                new ArrayList<>(),
                nodeEntity.getAdditionalInfo(),
                parent == null ? null : parent.getId(),
                Optional.ofNullable(nodeEntity.getWeight()).orElse(0L),
                Optional.ofNullable(nodeEntity.getIsCustom()).orElse(false),
                isNodeVisible(nodeEntity),
                Optional.ofNullable(nodeEntity.getDepartment()).map(department -> department.getId()).orElse(null),
                Optional.ofNullable(nodeEntity.getDepartment()).map(DepartmentEntity::getName).orElse(null)
        );
    }

    public boolean isNodeVisible(NodeEntity nodeEntity) {
        return !nodeEntity.getIsCustom() ||
                Optional.ofNullable(nodeEntity.getWeight()).orElse(0L) > VISIBILITY_THRESHOLD;
    }
}
