package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.TaskDto;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class TaskDtoConverter implements Converter<TaskDto, TaskEntity> {

    private final DepartmentRepository departmentRepository;
    private final UserRepository userRepository;

    @Override
    public TaskEntity convert(MappingContext<TaskDto, TaskEntity> mappingContext) {
        TaskEntity taskEntity = Optional.ofNullable(mappingContext.getDestination()).orElse(new TaskEntity());
        TaskDto taskDto = mappingContext.getSource();
        taskEntity.setName(taskDto.getName());
        taskEntity.setDescription(taskDto.getDescription());
        taskEntity.setCreationDate(taskDto.getCreationDate());
        taskEntity.setExpirationDate(taskDto.getExpirationDate());
        taskEntity.setStatus(taskDto.getStatus());
        taskEntity.setDepartment(departmentRepository.findById(taskDto.getDepartmentId()).orElseThrow());
        taskEntity.setUser(userRepository.findById(taskDto.getUserId()).orElseThrow());
        return taskEntity;
    }

}
