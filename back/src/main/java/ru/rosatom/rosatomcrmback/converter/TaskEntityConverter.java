package ru.rosatom.rosatomcrmback.converter;

import lombok.RequiredArgsConstructor;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.model.TaskDto;

import javax.transaction.Transactional;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Component
public class TaskEntityConverter implements Converter<TaskEntity, TaskDto> {

    @Override
    public TaskDto convert(MappingContext<TaskEntity, TaskDto> mappingContext) {
        TaskDto taskDto = Optional.ofNullable(mappingContext.getDestination()).orElse(new TaskDto());
        TaskEntity taskEntity = mappingContext.getSource();
        taskDto.setId(taskEntity.getId());
        taskDto.setName(taskEntity.getName());
        taskDto.setDescription(taskEntity.getDescription());
        taskDto.setCreationDate(taskEntity.getCreationDate());
        taskDto.setExpirationDate(taskEntity.getExpirationDate());
        taskDto.setUserId(taskEntity.getUser().getId());
        taskDto.setStatus(taskEntity.getStatus());
        taskDto.setDepartmentId(taskEntity.getDepartment().getId());
        return taskDto;
    }

}
