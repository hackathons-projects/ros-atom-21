package ru.rosatom.rosatomcrmback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NodeDto {

    private Long id;

    private String question;

    private String bundleText;

    private List<NodeDto> children;

    private String additionalInfo;

    private Long parentId;

    private Long weight;

    private Boolean isCustom;

    private Boolean isVisible;

    private Long targetServiceId;

    private String targetServiceName;
}
