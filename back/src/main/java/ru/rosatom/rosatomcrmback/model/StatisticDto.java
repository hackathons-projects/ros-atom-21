package ru.rosatom.rosatomcrmback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StatisticDto {

    private Float kpi;

    private Long doneTasks;

    private Long inProgress;

    private Long expired;
}
