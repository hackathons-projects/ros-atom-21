package ru.rosatom.rosatomcrmback.model;

import lombok.*;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;
import ru.rosatom.rosatomcrmback.domain.model.Role;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {

    private Long id;

    private String name;

    private String lastName;

    private String middleName;

    private LocalDate birthDay;

    private Boolean isOnline;

    private String email;

    private String phone;

    private String position;

    private String password;

    private Role role;

    private Long managerId;

    private Set<Long> personalIds = new HashSet<>();

    private Set<Long> tasksIds = new HashSet<>();

    private Long departmentId;
}
