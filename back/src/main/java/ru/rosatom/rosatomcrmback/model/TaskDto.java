package ru.rosatom.rosatomcrmback.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rosatom.rosatomcrmback.domain.model.TaskStatus;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TaskDto {

    private Long id;

    private String name;

    private String description;

    private Long departmentId;

    private Long userId;

    private LocalDateTime creationDate;

    private LocalDateTime expirationDate;

    private TaskStatus status;
}
