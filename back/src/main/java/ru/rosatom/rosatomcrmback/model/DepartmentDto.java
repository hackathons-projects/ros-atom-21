package ru.rosatom.rosatomcrmback.model;

import lombok.*;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DepartmentDto {

    private Long id;

    private String name;

    private String description;

    private Long directorId;

    private Set<Long> personalIds = new HashSet<>();

    private Set<Long> tasksIds = new HashSet<>();

}
