package ru.rosatom.rosatomcrmback.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.rosatom.rosatomcrmback.model.UserDto;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignInResponse {

    private String accessToken;

    private String refreshToken;

    private UserDto user;
}
