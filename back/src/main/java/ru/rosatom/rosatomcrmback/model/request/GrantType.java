package ru.rosatom.rosatomcrmback.model.request;

public enum GrantType {
    PASSWORD, REFRESH_TOKEN, TOKEN
}
