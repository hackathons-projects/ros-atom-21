package ru.rosatom.rosatomcrmback.domain.repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
}
