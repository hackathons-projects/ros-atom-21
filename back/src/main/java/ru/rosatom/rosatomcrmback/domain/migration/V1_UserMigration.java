package ru.rosatom.rosatomcrmback.domain.migration;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.*;
import ru.rosatom.rosatomcrmback.domain.repostory.TaskRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;

import java.time.LocalDateTime;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V1_UserMigration implements Migration {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TaskRepository taskRepository;

    @Override
    public String getId() {
        return V1_UserMigration.class.getName();
    }

    @Override
    public void migrate() {
        UserEntity admin = new UserEntity();
        admin.setEmail("admin@ros.atom");
        admin.setPassword(passwordEncoder.encode("admin"));
        admin.setRole(Role.ADMIN);
        admin = userRepository.save(admin);

        // Create test department
        DepartmentEntity department = new DepartmentEntity("Test dep", "", admin, new HashSet<>(), new HashSet<>());
        admin.setDepartment(department);
        admin = userRepository.save(admin);

        // Add admin employers
        for (int index = 0; index < 5; index++) {
            UserEntity emp = new UserEntity();
            emp.setRole(Role.USER);
            emp.setEmail("emp" + index + "@ros.atom");
            emp.setPassword(passwordEncoder.encode("emp"));
            emp.setManager(admin);
            emp.setDepartment(admin.getDepartment());
            admin.getPersonal().add(emp);
            emp = userRepository.save(emp);
        }

        admin = userRepository.save(admin);

        // Create test Tasks for admin
        for (int index = 0; index < 5; index++) {
            TaskEntity taskEntity = new TaskEntity();
            taskEntity.setUser(admin);
            taskEntity.setName("Test " + index);
            taskEntity.setDescription(" ");
            taskEntity.setStatus(TaskStatus.DONE);
            taskEntity.setCreationDate(LocalDateTime.now());
            taskEntity.setExpirationDate(LocalDateTime.now().plusDays(5));
            taskEntity.setDepartment(department);
            taskRepository.save(taskEntity);
        }
    }
}
