package ru.rosatom.rosatomcrmback.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.domain.model.MigrationEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.MigrationRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

@RequiredArgsConstructor
@Component
public class Migrator {

    private final List<Migration> dataBaseMigrations;
    private final MigrationRepository migrationRepository;
    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;

    @PostConstruct
    public void migrate() {
        dataBaseMigrations.stream()
                .filter(migration -> !migrationRepository.existsByClassName(migration.getId()))
                .sorted(Comparator.comparing(m -> m.getClass().getName()))
                .forEach(migration -> {
                    migration.migrate();
                    migrationRepository.save(new MigrationEntity(migration.getId()));
                });
    }

}
