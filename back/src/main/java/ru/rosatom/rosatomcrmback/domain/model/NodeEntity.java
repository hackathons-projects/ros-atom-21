package ru.rosatom.rosatomcrmback.domain.model;

import lombok.*;

import javax.persistence.*;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "nodes")
public class NodeEntity extends BaseEntity<Long> {

    @Column
    private String question;

    @Column
    private String bundleText;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_node_id")
    private NodeEntity parent;

    @Column
    private String additionalInfo;

    @Column
    private Long weight;

    @Column
    private Boolean isCustom = false;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    private DepartmentEntity department;
}
