package ru.rosatom.rosatomcrmback.domain.migration;

/**
 * Conventional naming by order
 */
public interface Migration {
    String getId();

    void migrate();
}
