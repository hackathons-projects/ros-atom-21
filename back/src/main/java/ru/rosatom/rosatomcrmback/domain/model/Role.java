package ru.rosatom.rosatomcrmback.domain.model;

public enum Role {
    USER, ADMIN
}
