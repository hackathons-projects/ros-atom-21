package ru.rosatom.rosatomcrmback.domain.repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rosatom.rosatomcrmback.domain.model.NodeEntity;

import java.util.List;

public interface NodeRepository extends JpaRepository<NodeEntity, Long> {

    NodeEntity findByParentNull();

    List<NodeEntity> findAllByParent(NodeEntity parent);
}
