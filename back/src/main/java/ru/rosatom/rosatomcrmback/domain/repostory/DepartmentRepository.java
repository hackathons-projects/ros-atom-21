package ru.rosatom.rosatomcrmback.domain.repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {
}
