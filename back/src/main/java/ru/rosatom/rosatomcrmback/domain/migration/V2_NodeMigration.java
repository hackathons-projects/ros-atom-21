package ru.rosatom.rosatomcrmback.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;
import ru.rosatom.rosatomcrmback.domain.model.NodeEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.NodeRepository;

@RequiredArgsConstructor
@Transactional
@Component
public class V2_NodeMigration implements Migration {

    private final NodeRepository nodeRepository;
    private final DepartmentRepository departmentRepository;

    @Override
    public String getId() {
        return V2_NodeMigration.class.getName();
    }

    @Override
    public void migrate() {
        DepartmentEntity department = departmentRepository.findById(1L).orElseThrow();
        NodeEntity node1 = nodeRepository.save(new NodeEntity(null, "Ваше обращение связано с …", null, null, null, false, null));

        NodeEntity node2 = nodeRepository.save(new NodeEntity("защита прав потребителей", "укажите категорию (вид…)", node1, null, null, false, null));
        nodeRepository.save(new NodeEntity("образование", null, node1, null, null, false, null));
        nodeRepository.save(new NodeEntity("здравоохранение", null, node1, null, null, false, null));
        nodeRepository.save(new NodeEntity("ЖКХ", null, node1, null, null, false, null));
        nodeRepository.save(new NodeEntity("транспорт", null, node1, null, null, false, null));
        nodeRepository.save(new NodeEntity("Деятельность гос.органов", null, node1, null, null, false, null));
        nodeRepository.save(new NodeEntity(null, null, node1, null, null, false, null));

        NodeEntity node3 = nodeRepository.save(new NodeEntity(null, "товар", node2, null, null, false, null));
        nodeRepository.save(new NodeEntity(null, "работа", node2, null, null, false, null));
        nodeRepository.save(new NodeEntity(null, "услуга", node2, null, null, false, null));

        nodeRepository.save(new NodeEntity(null, "качество товара", node3, null, null, false, null));
        nodeRepository.save(new NodeEntity(null, "Сроки оказания услуг (выполнения работ)", node3, null, null, false, null));
    }
}
