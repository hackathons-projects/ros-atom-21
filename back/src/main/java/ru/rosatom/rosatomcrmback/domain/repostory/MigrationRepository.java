package ru.rosatom.rosatomcrmback.domain.repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.rosatom.rosatomcrmback.domain.model.MigrationEntity;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
