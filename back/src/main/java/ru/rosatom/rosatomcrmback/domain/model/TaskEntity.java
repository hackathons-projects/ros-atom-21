package ru.rosatom.rosatomcrmback.domain.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "tasks")
public class TaskEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @Lob
    @Column
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    private DepartmentEntity department;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(nullable = false)
    private LocalDateTime creationDate;

    @Column
    private LocalDateTime expirationDate;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;
}
