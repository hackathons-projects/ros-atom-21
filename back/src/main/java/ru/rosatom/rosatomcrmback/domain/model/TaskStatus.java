package ru.rosatom.rosatomcrmback.domain.model;

public enum TaskStatus {
    CREATED, DONE, EXPIRED
}
