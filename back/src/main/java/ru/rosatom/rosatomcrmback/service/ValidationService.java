package ru.rosatom.rosatomcrmback.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.exception.CustomException;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Slf4j
@Transactional
@Service
public class ValidationService {

    private final UserRepository userRepository;
    private final DateTimeFormatter timeStampFormatter;
    private final AuthService authService;

    public ValidationService(UserRepository userRepository,
                             @Value("${format.local-date-time}") String timestampFormat,
                             @Lazy AuthService authService) {
        this.userRepository = userRepository;
        this.timeStampFormatter = DateTimeFormatter.ofPattern(timestampFormat);
        this.authService = authService;
    }

    public void validateAdminOrOwner(Long id) {
        if (!(authService.isAdmin() || authService.isOwner(id))) {
            throw new CustomException("Access denied", HttpStatus.FORBIDDEN);
        }
    }

    public void validatePassword(String password) {
        if (Optional.ofNullable(password).orElseThrow().length() < 6) {
            throw new CustomException("Password must be 6 or more characters", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void userExists(String email) {
        if (userRepository.findByEmail(email).isPresent()) {
            throw new CustomException("An account for this email already exists", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
