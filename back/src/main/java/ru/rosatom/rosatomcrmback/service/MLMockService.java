package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.TaskStatus;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.TaskDto;

import java.time.LocalDateTime;
import java.util.UUID;

@RequiredArgsConstructor
@Transactional
@Service
public class MLMockService {
    private final UserRepository userRepository;

    // TODO integration with ML
    public TaskDto taskClassification(String text) {
        UserEntity user = getUserWithOptimalScoringForTask();
        TaskDto taskDto = new TaskDto();
        taskDto.setUserId(user.getId());
        taskDto.setStatus(TaskStatus.CREATED);
        taskDto.setName("Classified task:  " + UUID.randomUUID());
        taskDto.setDescription(text);
        taskDto.setCreationDate(LocalDateTime.now());
        taskDto.setExpirationDate(LocalDateTime.now().plusDays(5));
        taskDto.setDepartmentId(user.getDepartment().getId());
        return taskDto;
    }

    // TODO get optimal user for task
    private UserEntity getUserWithOptimalScoringForTask() {
        return userRepository.findByEmail("admin@ros.atom").orElseThrow();
    }
}
