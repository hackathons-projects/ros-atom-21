package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.DepartmentEntity;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.TaskRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.TaskDto;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@RequiredArgsConstructor
@Service
public class TaskService {

    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;
    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public List<TaskDto> getAllTasksByUser(Long userId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow();
        return userEntity.getTasks().stream().map(task -> modelMapper.map(task, TaskDto.class)).collect(Collectors.toList());
    }

    public List<TaskDto> getAllTasksByDepartment(Long departmentId) {
        DepartmentEntity department = departmentRepository.findById(departmentId).orElseThrow();
        return department.getTasks().stream().map(task -> modelMapper.map(task, TaskDto.class)).collect(Collectors.toList());
    }

    public TaskDto createTask(TaskDto taskDto) {
        TaskEntity map = modelMapper.map(taskDto, TaskEntity.class);
        TaskEntity saved = taskRepository.save(map);
        return modelMapper.map(saved, TaskDto.class);
    }
}
