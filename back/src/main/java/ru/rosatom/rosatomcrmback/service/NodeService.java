package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.NodeEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.NodeRepository;
import ru.rosatom.rosatomcrmback.model.NodeDto;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class NodeService {

    private final NodeRepository nodeRepository;
    private final ModelMapper modelMapper;

    public NodeDto getStructureTree() {
        NodeEntity rootNode = nodeRepository.findByParentNull();
        return createStructure(rootNode);
    }

    private NodeDto createStructure(NodeEntity nodeEntity) {
        NodeDto node = modelMapper.map(nodeEntity, NodeDto.class);

        List<NodeEntity> children = nodeRepository.findAllByParent(nodeEntity);
        children.forEach(child -> {
            NodeDto structure = createStructure(child);
            node.getChildren().add(structure);
        });

        return node;
    }

    public NodeDto createCustomNode(NodeDto nodeDto) {
        NodeEntity customNode = new NodeEntity();
        NodeEntity saved = saveOrUpdate(customNode, nodeDto);
        saved.setIsCustom(true);
        saved.setWeight(1L);
        return modelMapper.map(saved, NodeDto.class);
    }

    public NodeDto updateNode(NodeDto nodeDto) {
        NodeEntity customNode = nodeRepository.findById(nodeDto.getId()).orElseThrow(() -> new RuntimeException("Not Found"));
        NodeEntity saved = saveOrUpdate(customNode, nodeDto);
        return modelMapper.map(saved, NodeDto.class);
    }

    private NodeEntity saveOrUpdate(NodeEntity customNode, NodeDto nodeDto) {
        customNode.setQuestion(nodeDto.getQuestion());
        customNode.setBundleText(nodeDto.getBundleText());
        customNode.setAdditionalInfo(nodeDto.getAdditionalInfo());

        NodeEntity byId = nodeRepository.findById(nodeDto.getParentId()).orElseThrow();
        customNode.setParent(byId);
        return nodeRepository.save(customNode);
    }

    public Long incrementWeight(Long nodeId) {
        NodeEntity customNode = nodeRepository.findById(nodeId).orElseThrow(() -> new RuntimeException("Not Found"));
        if (customNode.getIsCustom()) {
            customNode.setWeight(customNode.getWeight() + 1);
            NodeEntity saved = nodeRepository.save(customNode);
            return saved.getWeight();
        } else {
            throw new RuntimeException("Not allowed increment not custom node");
        }
    }
}
