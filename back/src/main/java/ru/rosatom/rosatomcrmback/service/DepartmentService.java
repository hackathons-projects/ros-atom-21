package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.DepartmentDto;

@Transactional
@RequiredArgsConstructor
@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;

    public Page<DepartmentDto> getAllDepartments(Pageable pageable) {
        return departmentRepository.findAll(pageable).map(department -> modelMapper.map(department, DepartmentDto.class));
    }

    public DepartmentDto getDepartment(Long departmentId) {
        return modelMapper.map(departmentRepository.findById(departmentId), DepartmentDto.class);
    }

    public DepartmentDto getUserDepartment(Long userId) {
        return modelMapper.map(userRepository.findById(userId).orElseThrow().getDepartment(), DepartmentDto.class);
    }
}
