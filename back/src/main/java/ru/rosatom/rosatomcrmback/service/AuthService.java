package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.configuration.security.JwtTokenProvider;
import ru.rosatom.rosatomcrmback.domain.model.Role;
import ru.rosatom.rosatomcrmback.domain.model.UserEntity;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.exception.CustomException;
import ru.rosatom.rosatomcrmback.model.UserDto;
import ru.rosatom.rosatomcrmback.model.request.SignInRequest;
import ru.rosatom.rosatomcrmback.model.response.SignInResponse;

import java.util.HashSet;

@Transactional
@RequiredArgsConstructor
@Service
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final ModelMapper modelMapper;
    private final ValidationService validationService;
    private final DepartmentRepository departmentRepository;

    public UserDto getCurrentUser() {
        try {
            return (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception ignored) {
            throw new CustomException("Not Authorized", HttpStatus.FORBIDDEN);
        }
    }

    public boolean isAuthorized() {
        return !"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public boolean isAdmin() {
        return Role.ADMIN.equals(getCurrentUser().getRole());
    }

    public boolean isOwner(Long userId) {
        return userId.equals(getCurrentUser().getId());
    }

    public String passwordHash(String password) {
        return passwordEncoder.encode(password);
    }

    public SignInResponse login(SignInRequest signInRequest) {
        UserEntity user;
        switch (signInRequest.getGrantType()) {
            case PASSWORD:
                user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                if (passwordEncoder.matches(signInRequest.getPassword(), user.getPassword())) {
                    UserDto userDto = modelMapper.map(user, UserDto.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getId().toString(), userDto);
                    String refreshToken = jwtTokenProvider.createRefreshToken(user.getId().toString());
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            case REFRESH_TOKEN:
                if (signInRequest.getRefreshToken() != null && jwtTokenProvider.validateToken(signInRequest.getRefreshToken())) {
                    String username = jwtTokenProvider.getUsername(signInRequest.getRefreshToken());
                    user = userRepository.findByEmail(username).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                    UserDto userDto = modelMapper.map(user, UserDto.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getId().toString(), userDto);
                    String refreshToken = signInRequest.getRefreshToken();
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            case TOKEN:
                if (signInRequest.getToken() != null && jwtTokenProvider.validateToken(signInRequest.getToken())) {
                    return new SignInResponse(signInRequest.getToken(), signInRequest.getRefreshToken(),
                            jwtTokenProvider.getUser(signInRequest.getToken()));
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            default:
                throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
        }

    }

    public UserDto createUser(UserDto userDto) {
        validationService.userExists(userDto.getEmail());
        validationService.validatePassword(userDto.getPassword());

        return modelMapper.map(userRepository.save(new UserEntity(
                userDto.getName(), userDto.getLastName(), userDto.getMiddleName(), userDto.getBirthDay(),
                userDto.getIsOnline(), userDto.getEmail(), userDto.getPhone(), userDto.getPosition(),
                passwordEncoder.encode(userDto.getPassword()), userDto.getRole(),
                userRepository.findById(userDto.getManagerId()).orElse(null),
                new HashSet<>(), new HashSet<>(),
                departmentRepository.findById(userDto.getDepartmentId()).orElse(null)
        )), UserDto.class);
    }
}
