package ru.rosatom.rosatomcrmback.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rosatom.rosatomcrmback.domain.model.TaskEntity;
import ru.rosatom.rosatomcrmback.domain.model.TaskStatus;
import ru.rosatom.rosatomcrmback.domain.repostory.DepartmentRepository;
import ru.rosatom.rosatomcrmback.domain.repostory.UserRepository;
import ru.rosatom.rosatomcrmback.model.StatisticDto;

import java.util.Set;

@RequiredArgsConstructor
@Transactional
@Service
public class StatisticsService {

    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;

    public StatisticDto getStatsByUser(Long userId) {
        Set<TaskEntity> tasks = userRepository.findById(userId).orElseThrow().getTasks();
        return getStatistics(tasks);
    }

    public StatisticDto getStatsByDepartment(Long departmentId) {
        Set<TaskEntity> tasks = departmentRepository.findById(departmentId).orElseThrow().getTasks();
        return getStatistics(tasks);
    }

    private StatisticDto getStatistics(Set<TaskEntity> tasks) {
        long done = tasks.stream().filter(task -> TaskStatus.DONE.equals(task.getStatus())).count();
        long inProgress = tasks.stream().filter(task -> TaskStatus.CREATED.equals(task.getStatus())).count();
        long expired = tasks.stream().filter(task -> TaskStatus.EXPIRED.equals(task.getStatus())).count();
        return new StatisticDto(scoring(done, inProgress, expired, tasks.size()), done, inProgress, expired);
    }

    // TODO ML SCORING
    private float scoring(long done, long inProgress, long expired, int count) {
        return done * 1f / count + inProgress * 0.6f / count - expired * 0.8f / count;
    }
}
