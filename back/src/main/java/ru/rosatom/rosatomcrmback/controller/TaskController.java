package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rosatom.rosatomcrmback.model.TaskDto;
import ru.rosatom.rosatomcrmback.service.TaskService;

import java.util.List;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @GetMapping("/user/{userId}")
    public ResponseEntity<List<TaskDto>> getTaskByUser(@PathVariable Long userId) {
        return ResponseEntity.ok(taskService.getAllTasksByUser(userId));
    }

    @GetMapping("/department/{departmentId}")
    public ResponseEntity<List<TaskDto>> getTaskByDepartment(@PathVariable Long departmentId) {
        return ResponseEntity.ok(taskService.getAllTasksByDepartment(departmentId));
    }

}
