package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rosatom.rosatomcrmback.model.DepartmentDto;
import ru.rosatom.rosatomcrmback.service.DepartmentService;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/departments")
public class DepartmentController {

    private final DepartmentService departmentService;

    @GetMapping
    public ResponseEntity<Page<DepartmentDto>> getAllDepartments(Pageable pageable) {
        return ResponseEntity.ok(departmentService.getAllDepartments(pageable));
    }

    @GetMapping("/{departmentId}")
    public ResponseEntity<DepartmentDto> getAllDepartment(@PathVariable Long departmentId) {
        return ResponseEntity.ok(departmentService.getDepartment(departmentId));
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<DepartmentDto> getUserDepartment(@PathVariable Long userId) {
        return ResponseEntity.ok(departmentService.getUserDepartment(userId));
    }
}
