package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.rosatom.rosatomcrmback.model.UserDto;
import ru.rosatom.rosatomcrmback.service.UserService;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    /**
     * [ADMIN ONLY]
     * <p>
     * GET - Get all users, or a page at a time.
     * ex. Paginate with ?page=0&size=100
     */
    @GetMapping
    public ResponseEntity<Page<UserDto>> getAllUsers(Pageable pageable) {
        return ResponseEntity.ok(userService.getAllUsers(pageable));
    }

    /**
     * [OWNER/ADMIN]
     * <p>
     * GET - Get a specific user.
     */
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }

    /**
     * Update a user's password.
     * {
     * oldPassword: STRING,
     * newPassword: STRING
     * }
     */
    @PutMapping("/{id}/password")
    public ResponseEntity<UserDto> changeUserPassword(@PathVariable Long id,
                                                      @Valid @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$") String password) {
        return ResponseEntity.ok(userService.updatePassword(id, password));
    }

}
