package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rosatom.rosatomcrmback.model.UserDto;
import ru.rosatom.rosatomcrmback.model.request.SignInRequest;
import ru.rosatom.rosatomcrmback.model.response.SignInResponse;
import ru.rosatom.rosatomcrmback.service.AuthService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    /**
     * Login a user with a username (email) and password.
     * Find em', check em'.
     * Pass them an authentication token on success.
     * Otherwise, 401. You fucked up.
     */
    @PreAuthorize("permitAll()")
    @PostMapping("/login")
    public ResponseEntity<SignInResponse> login(@RequestBody SignInRequest signInRequest) {
        return ResponseEntity.ok(authService.login(signInRequest));
    }

    /**
     * Register a user with a username (email) and password.
     * If it already exists, then don't register, duh.
     * <p>
     * body {
     * email: email,
     * password: password
     * }
     */
    @PostMapping("/register")
    public ResponseEntity<UserDto> register(@RequestBody UserDto userRegistrationRequest) {
        return ResponseEntity.ok(authService.createUser(userRegistrationRequest));
    }
}
