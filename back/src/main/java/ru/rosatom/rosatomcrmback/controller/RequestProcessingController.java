package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rosatom.rosatomcrmback.model.TaskDto;
import ru.rosatom.rosatomcrmback.service.MLMockService;
import ru.rosatom.rosatomcrmback.service.TaskService;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/requests")
public class RequestProcessingController {

    private final TaskService taskService;
    private final MLMockService mlMockService;

    @PostMapping
    public ResponseEntity<TaskDto> createTask(@RequestBody String requestText) {
        TaskDto taskDto = mlMockService.taskClassification(requestText);
        return ResponseEntity.ok(taskService.createTask(taskDto));
    }
}
