package ru.rosatom.rosatomcrmback.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rosatom.rosatomcrmback.model.StatisticDto;
import ru.rosatom.rosatomcrmback.service.StatisticsService;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping("/user/{userId}")
    public ResponseEntity<StatisticDto> getTaskByUser(@PathVariable Long userId) {
        return ResponseEntity.ok(statisticsService.getStatsByUser(userId));
    }

    @GetMapping("/department/{departmentId}")
    public ResponseEntity<StatisticDto> getTaskByDepartment(@PathVariable Long departmentId) {
        return ResponseEntity.ok(statisticsService.getStatsByDepartment(departmentId));
    }

}
