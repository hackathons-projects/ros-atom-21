package ru.rosatom.rosatomcrmback.configuration.camel;

import lombok.RequiredArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class FileRouteBuilder extends RouteBuilder {

    private final FileProcessor fileProcessor;

    @Override
    public void configure() throws Exception {
        from("file:input").process(fileProcessor);
    }
}