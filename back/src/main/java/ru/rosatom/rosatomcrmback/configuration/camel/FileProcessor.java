package ru.rosatom.rosatomcrmback.configuration.camel;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import ru.rosatom.rosatomcrmback.model.TaskDto;
import ru.rosatom.rosatomcrmback.service.MLMockService;
import ru.rosatom.rosatomcrmback.service.TaskService;

@Slf4j
@RequiredArgsConstructor
@Component
public class FileProcessor implements Processor {

    private final TaskService taskService;
    private final MLMockService mlMockService;

    @Override
    public void process(Exchange exchange) throws Exception {
        String originalFileContent = exchange.getIn().getBody(String.class);
        TaskDto taskDto = mlMockService.taskClassification(originalFileContent);
        TaskDto task = taskService.createTask(taskDto);
        log.info("Task created: " + task.toString());
    }
}
