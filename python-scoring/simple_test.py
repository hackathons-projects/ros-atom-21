import sys, wget, os
import gensim, logging
from flask import Flask, abort, request

import preprocessing.rus_preprocessing_udpipe as ppu

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

import zipfile

# TODO download before first start
# model_url = 'http://vectors.nlpl.eu/repository/11/180.zip'
# m = wget.download(model_url)
# model_file = model_url.split('/')[-1]

model_file = "180.zip"
with zipfile.ZipFile(model_file, 'r') as archive:
    stream = archive.open('model.bin')
    model = gensim.models.KeyedVectors.load_word2vec_format(stream, binary=True)

app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World!"

@app.route('/similarity', methods=['POST'])
def similarity():
    if not request.json:
        abort(400)

    ppu.standard_library.install_aliases()
    # URL of the UDPipe model
    udpipe_model_url = 'https://rusvectores.org/static/models/udpipe_syntagrus.model'
    udpipe_filename = udpipe_model_url.split('/')[-1]

    if not os.path.isfile(udpipe_filename):
        print('UDPipe model not found. Downloading...', file=sys.stderr)
        wget.download(udpipe_model_url)

    print('\nLoading the model...', file=sys.stderr)
    model = ppu.Model.load(udpipe_filename)
    process_pipeline = ppu.Pipeline(model, 'tokenize', ppu.Pipeline.DEFAULT, ppu.Pipeline.DEFAULT, 'conllu')

    test = prepare_string(process_pipeline, request.json['test'])
    store = []
    for store_record in request.json['store']:
        store.append(prepare_string(process_pipeline, store_record))

    max_rate = 0
    optimal_from_storage = ''

    for s in store:
        rate = simpleModelComparison(test, s)
        if rate > max_rate:
            max_rate = rate
            optimal_from_storage = s

    result = {'test': test, 'optimal': optimal_from_storage, 'rate': max_rate}

    return str(result)

# Simple model
def simpleModelComparison(test, s):
    return compare_strings(test, s)

def prepare_string(process_pipeline, s1):
    output = ppu.process(process_pipeline, text=s1)
    return ' '.join(output)


def compare_strings(s1, s2):
    arr1 = s1.split(' ')
    arr2 = s2.split(' ')

    successCount = 0
    for word1 in arr1:
        for word2 in arr2:
            try:
                if model.similarity(word1, word2) > 0.6:
                    successCount += 1
            except:
                successCount -= 0.25

    return successCount / len(arr1)


# TODO testing and integration damerau realization in future releases
# Damerau

def damerauComparison(test, s):
    s1 = test.split(' ')
    s2 = s.split(' ')

    return damerau_levenshtein_distance(s1, s2)

def damerau_levenshtein_distance(s1, s2):
    d = {}
    lenstr1 = len(s1)
    lenstr2 = len(s2)
    for i in range(-1, lenstr1 + 1):
        d[(i, -1)] = i + 1
    for j in range(-1, lenstr2 + 1):
        d[(-1, j)] = j + 1

    for i in range(lenstr1):
        for j in range(lenstr2):
            isSimilar = False

            try:
                if model.similarity(s1[i], s2[j]) < 0.6:
                    isSimilar = True
            except:
                isSimilar = False

            if isSimilar: # s1[i] == s2[j]:  # similarity
                cost = 0
            else:
                cost = 1
            d[(i, j)] = min(
                d[(i - 1, j)] + 1,  # deletion
                d[(i, j - 1)] + 1,  # insertion
                d[(i - 1, j - 1)] + cost,  # substitution
            )
            if i and j and s1[i] == s2[j - 1] and s1[i - 1] == s2[j]:
                d[(i, j)] = min(d[(i, j)], d[i - 2, j - 2] + cost)  # transposition

    return d[lenstr1 - 1, lenstr2 - 1]

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5555)