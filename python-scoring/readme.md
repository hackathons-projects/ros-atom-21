# Example

    GET http://localhost:5555/similarity 

Request

    {
        "test": "Мама мыла раму",
        "store": [
            "Мама била раму",
            "Кто-то мыла раму",
            "Мама мила раму"
            ]
    }

Result 

    {'rate': 0.6666666666666666, 'optimal': 'мама_PROPN бить_VERB рама_NOUN', 'test': 'мама_PROPN мыть_VERB рама_NOUN'}